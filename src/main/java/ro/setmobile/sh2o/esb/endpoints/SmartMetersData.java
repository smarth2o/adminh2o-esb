/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/AdminSH2O/SmartMetersData")
public class SmartMetersData {

	@GET
	@Path("/getSmartMeters")
	@Produces(MediaType.APPLICATION_JSON)
	public String getSmartMeters() {
		return null;
	}
}
