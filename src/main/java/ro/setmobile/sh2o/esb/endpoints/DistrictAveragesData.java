/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/AdminSH2O/DistrictAveragesData")
public class DistrictAveragesData {

	@GET
	@Path("/getDistrictAverages")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDistrictAverages(@QueryParam("district_id") String district_id) {
		return null;
	}
}
