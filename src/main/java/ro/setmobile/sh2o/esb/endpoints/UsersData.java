/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/AdminSH2O/UsersData")
public class UsersData {

	@GET
	@Path("/getUsers")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUsers() {
		return null;
	}
}
