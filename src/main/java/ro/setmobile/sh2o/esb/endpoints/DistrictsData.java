/**
 * 
 */
package ro.setmobile.sh2o.esb.endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * @author dan
 *
 */
@Path("/AdminSH2O/DistrictsData")
public class DistrictsData {

	@GET
	@Path("/getDistricts")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDistricts() {
		return null;
	}
}
