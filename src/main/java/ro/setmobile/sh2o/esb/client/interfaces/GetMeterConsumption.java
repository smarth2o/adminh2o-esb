package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetMeterConsumptionData")
public interface GetMeterConsumption {


	@GET
	@Path("/getMeterConsumptionData")
	@Produces(MediaType.APPLICATION_JSON)
	public String getMeterConsumptionData(@QueryParam("meter_id") String meter_id);
}
