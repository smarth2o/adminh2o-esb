package ro.setmobile.sh2o.esb.client.interfaces;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/GetDistrictAverages")
public interface GetDistrictAverages {


	@GET
	@Path("/getDistrictAverages")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDistrictAverages(@QueryParam("district_id") String district_id);
}
